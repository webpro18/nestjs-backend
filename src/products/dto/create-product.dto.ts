import { Column, PrimaryGeneratedColumn } from 'typeorm';

export class CreateProductDto {
  @PrimaryGeneratedColumn({ name: 'product_id' })
  id: number;

  @Column({ name: 'product_name' })
  name: string;

  @Column({ name: 'product_price' })
  price: number;
}
