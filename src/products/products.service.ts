import { Injectable } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { InjectRepository } from '@nestjs/typeorm/dist/common/typeorm.decorators';
import { Product } from './entities/product.entity';
import { Repository } from 'typeorm';

@Injectable()
export class ProductsService {
  constructor(
    @InjectRepository(Product)
    private productReponsitory: Repository<Product>,
  ) {}

  create(createProductDto: CreateProductDto): Promise<Product> {
    const product: Product = new Product();
    product.id = createProductDto.id;
    product.name = createProductDto.name;
    product.price = createProductDto.price;
    return this.productReponsitory.save(product);
  }

  findAll(): Promise<Product[]> {
    return this.productReponsitory.find();
  }

  findOne(id: number): Promise<Product> {
    return this.productReponsitory.findOneBy({ id: id });
  }

  async update(id: number, updateProductDto: UpdateProductDto) {
    const product = await this.productReponsitory.findOneBy({ id: id });
    const updateProduct = { ...product, ...updateProductDto };
    return this.productReponsitory.save(updateProduct);
  }

  async remove(id: number) {
    const product = await this.productReponsitory.findOneBy({ id: id });
    return this.productReponsitory.remove(product);
  }
}
