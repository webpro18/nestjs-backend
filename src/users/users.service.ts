import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { InjectRepository } from '@nestjs/typeorm/dist/common';
import { Repository } from 'typeorm';
import { User } from './entities/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersReponsitory: Repository<User>,
  ) {}
  create(createUserDto: CreateUserDto): Promise<User> {
    const user: User = new User();
    user.login = createUserDto.login;
    user.name = createUserDto.name;
    user.password = createUserDto.password;
    return this.usersReponsitory.save(user);
  }

  findAll(): Promise<User[]> {
    return this.usersReponsitory.find();
  }

  findOne(id: number): Promise<User> {
    return this.usersReponsitory.findOneBy({ id: id });
  }

  async update(id: number, updateUserDto: UpdateUserDto) {
    const user = await this.usersReponsitory.findOneBy({ id: id });
    const updateUser = { ...user, ...updateUserDto };
    return this.usersReponsitory.save(updateUser);
  }

  async remove(id: number) {
    const user = await this.usersReponsitory.findOneBy({ id: id });
    return this.usersReponsitory.remove(user);
  }
}
